#ifndef ROCK_SERVER_H
#define ROCK_SERVER_H

#include "Rock.h"
#include "NetworkManagerServer.h"



class RockServer : public Rock
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new RockServer() ); }
	virtual void HandleDying() override;

	virtual void Update() override;

	

	void TakeDamage( int inDamagingRockId );

protected:
	RockServer();

private:

	

	float		mTimeOfNextShot;
	float		mTimeBetweenShots;



};

#endif // ROCK_SERVER_H
