#include "BulletServer.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

BulletServer::BulletServer() :
	mTimeOfNextShot( 0.f ),
	mTimeBetweenShots( 0.2f )

{
	bulletTimer = Timing::sInstance.GetFrameStartTime() + 1.0f;
}

void BulletServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

/*! \brief This processes the input from clients and updates their position */
void BulletServer::Update()
{
	Bullet::Update();
	ProcessCollisionsWithScreenWalls();
	if (Timing::sInstance.GetFrameStartTime() >= bulletTimer)
	{
		SetDoesWantToDie(true);
	}
}

void BulletServer::TakeDamage( int inDamagingPlayerId )
{
	mHealth--;
	if( mHealth <= 0.f )
	{
		//and you want to die
		SetDoesWantToDie( true );
	}
}
