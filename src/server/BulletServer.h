#ifndef BULLET_SERVER_H
#define BULLET_SERVER_H

#include "Bullet.h"
#include "NetworkManagerServer.h"


class BulletServer : public Bullet
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new BulletServer() ); }
	virtual void HandleDying() override;

	virtual void Update() override;

	void TakeDamage( int inDamagingPlayerId );

protected:
	BulletServer();

private:

		float bulletTimer = 1;


	float		mTimeOfNextShot;
	float		mTimeBetweenShots;

};

#endif // PLAYER_SERVER_H
