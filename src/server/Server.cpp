
#include "Server.h"
#include "GameObjectRegistry.h"
#include "StringUtils.h"
#include "Colors.h"
#include "PlayerServer.h"
#include "RockServer.h"
#include "BulletServer.h"




bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}

// Registers the object on the server side//
Server::Server()
{

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'PLYR', PlayerServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction('ROCK', RockServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BLLT', BulletServer::StaticCreate);


	InitNetworkManager();

	// Setup latency
	float latency = 0.0f;
	string latencyString = StringUtils::GetCommandLineArg( 2 );
	if( !latencyString.empty() )
	{
		latency = stof( latencyString );
	}
	NetworkManagerServer::sInstance->SetSimulatedLatency( latency );
}

// Calls set up world then tells the engine to run//
int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

//Initialises network manager server ready for use //
bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg( 1 );
	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}

// initialises static game objects e.g. Rock//
void Server::SetupWorld()

{
	// Static game objects and NPCs here.
	SpawnRock(Vector3(2.0f, 2.0f, 2.0f));
	SpawnRock(Vector3(1.0f, 1.0f, 1.0f));
	SpawnRock(Vector3(1.0f, 1.0f, 1.0f));
	SpawnRock(Vector3(1.0f, 1.0f, 1.0f));
	SpawnRock(Vector3(1.0f, 1.0f, 1.0f));
	SpawnRock(Vector3(1.0f, 1.0f, 1.0f));
	SpawnRock(Vector3(1.0f, 1.0f, 1.0f));
	SpawnRock(Vector3(1.0f, 1.0f, 1.0f));
	SpawnRock(Vector3(3.0f, 3.0f, 1.0f));
}
// Creates a rock and sets its location//
void Server::SpawnRock(Vector3 Location)
{
	RockPtr rock = std::static_pointer_cast<Rock>(GameObjectRegistry::sInstance->CreateGameObject('ROCK'));
	rock->SetLocation(Location);
	
}
//  Handles incoming packets, checks for player statuses and tells the engine to update/ sends outgoing packets to clients //
void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnPlayers();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
}
// When a player joins they are assigned a player id, and SpawnPlayer is called.//
void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{

	int playerId = inClientProxy->GetPlayerId();

	
	SpawnPlayer( playerId );
}
// creates a player, sets their id and location//
void Server::SpawnPlayer( int inPlayerId )
{
	PlayerPtr player = std::static_pointer_cast< Player >( GameObjectRegistry::sInstance->CreateGameObject( 'PLYR' ) );
	player->SetColor( Colors::Red );//ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	player->SetPlayerId( inPlayerId );
	//gotta pick a better spawn location than this...
	player->SetLocation( Vector3( 1.f - static_cast< float >( inPlayerId ), 0.f, 0.f ) );

}
// if a player disconnects tells the server to remove the player//
void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's player
	int playerId = inClientProxy->GetPlayerId();

	PlayerPtr player = GetPlayer( playerId );
	if( player )
	{
		player->SetDoesWantToDie( true );
	}
}
// Searches for player if needed by other methods //
PlayerPtr Server::GetPlayer( int inPlayerId )
{
	//run through the objects till we find the Player...
	//it would be nice if we kept a pointer to the Player on the clientproxy
	//but then we'd have to clean it up when the Player died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];



		uint32_t type = go->GetClassId();

		//Player* player = dynamic_cast<Player*>(*go);
		PlayerPtr player = nullptr;
		if(type == 'PLYR')
		{
			player = std::static_pointer_cast< Player >(go);
		}

		if(player && player->GetPlayerId() == inPlayerId )
		{
			return player;
		}
	}

	return nullptr;

}
