#include "BulletClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"

BulletClient::BulletClient() :
	mTimeLocationBecameOutOfSync( 0.f ),
	mTimeVelocityBecameOutOfSync( 0.f )
{
	mSpriteComponent.reset( new SpriteComponent( this ) );
	mSpriteComponent->SetTexture( TextureManager::sInstance->GetTexture( "Bullet" ) );
}

void BulletClient::HandleDying()
{
	Bullet::HandleDying();

}


void BulletClient::Update()
{
	//for now, we don't simulate any movement on the client side
	//we only move when the server tells us to move
}

/*! \brief Deserialisation is reading from the BitStream in the same order it was written*/
void BulletClient::Read( InputMemoryBitStream& inInputStream )
{
	uint32_t shooterId;
	inInputStream.Read(shooterId);
	SetShooterID(shooterId);

	float oldRotation = GetRotation();
	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();

	float replicatedRotation;
	Vector3 replicatedLocation;
	Vector3 replicatedVelocity;

	
	inInputStream.Read( replicatedVelocity.mX );
	inInputStream.Read( replicatedVelocity.mY );

	SetVelocity( replicatedVelocity );

	inInputStream.Read( replicatedLocation.mX );
	inInputStream.Read( replicatedLocation.mY );

	SetLocation( replicatedLocation );

	inInputStream.Read( replicatedRotation );
	SetRotation( replicatedRotation );

	bool thrust;

	inInputStream.Read( thrust );
	mThrustDir = thrust ? 1.f : -1.f;
	
	
	Vector3 color;
	inInputStream.Read( color );
	SetColor( color );
	
	mHealth = 0;
	inInputStream.Read( mHealth, 4 );
}
