#ifndef PLAYER_TESTHARNESS_H_
#define PLAYER_TESTHARNESS_H_

#include <limits.h>
#include <gtest/gtest.h>

#include "Rock.h"

class RockTestHarness : public ::testing::Test
{
protected:

  virtual void SetUp();
  virtual void TearDown();

  RockPtr pp;

public:

    RockTestHarness();
    virtual ~RockTestHarness();
};

#endif // PLAYER_TESTHARNESS_H_
