#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "BulletTestHarness.h"
#include "Player.h"
#include "PlayerClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

BulletTestHarness::BulletTestHarness()
{
  pp = nullptr;
}

BulletTestHarness::~BulletTestHarness()
{
  pp.reset();
}
// ensures set up for tests //
void BulletTestHarness::SetUp()
{
    GameObject*	go = Bullet::StaticCreate();
    Bullet* b = static_cast<Bullet*>(go);
    this->pp.reset(b);
}
// ensures objects are reset //
void BulletTestHarness::TearDown()
{
    this->pp.reset();
    this->pp = nullptr;
}
// Uses google framework to compare object values against expected values, if all tests are true the test is passed. //
TEST_F(BulletTestHarness,Bulletconstructor_noArgs)
{
  // Check defaults are set
  // Should be no need to do these as they were tested with the base class.
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(pp->GetCollisionRadius(), 0.5f);
  EXPECT_FLOAT_EQ(pp->GetScale(),1.0f);
  EXPECT_FLOAT_EQ(pp->GetRotation(),0.0f);
  EXPECT_EQ(pp->GetIndexInWorld(), -1);
  EXPECT_EQ(pp->GetNetworkId(), 0);

  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetVelocity(), Vector3::Zero));
  // EXPECT_EQ(pp->GetBulletId(), 0.0f);

  //Initial state is update all
  int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(pp->GetAllStateMask(), check);

  //Check our macro has worked.
  EXPECT_EQ(pp->GetClassId(), 'BLLT');
  EXPECT_NE(pp->GetClassId(), 'HELP');


  //Added some getters so I could check these - not an easy class to test.
  EXPECT_FLOAT_EQ(pp->GetMaxLinearSpeed(),  50.0f);
  EXPECT_FLOAT_EQ(pp->GetMaxRotationSpeed(), 5.0f);
  EXPECT_FLOAT_EQ(pp->GetWallRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(pp->GetNPCRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(pp->GetLastMoveTimestamp(), 0.0f);
  EXPECT_FLOAT_EQ(pp->GetThrustDir(), 0.0f);
 // EXPECT_EQ(pp->GetHealth(), 3);
  //EXPECT_FALSE(pp->IsShooting());
}


/* Tests Omitted
* There's a good chunk of this which cannot be tested in this limited example,
* however there should be enough to underake some testing of the serialisation code.
*/

TEST_F(BulletTestHarness,EqualsOperator1)
{ /* Won't compile - why?
  Player a ();
  Player b ();

  a.SetPlayerId(10);
  b.SetPlayerId(10);

  EXPECT_TRUE(a == b);*/
}
// Compare if value is the same //
TEST_F(BulletTestHarness,BulletEqualsOperator2)
{
  Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());
  Bullet *b = static_cast<Bullet*>(Bullet::StaticCreate());

  a->SetShooterID(10);
  b->SetShooterID(10);

  EXPECT_TRUE(*a == *b);
}



TEST_F(BulletTestHarness,BulletEqualsOperator3)
{
  Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());
  Bullet *b = static_cast<Bullet*>(Bullet::StaticCreate());

  a->SetShooterID(10);
  b->SetShooterID(30);

  EXPECT_FALSE(*a == *b);
}

TEST_F(BulletTestHarness,BulletEqualsOperator4)
{
  BulletPtr b(static_cast<Bullet*>(Bullet::StaticCreate()));

  pp->SetShooterID(10);
  b->SetShooterID(10);

  EXPECT_TRUE(*pp == *b);
}

///* Serialistion tests in MemoryBitStreamTestHarness */