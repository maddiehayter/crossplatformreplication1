#include "Bullet.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "Timing.h"
#include "Player.h"

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Bullet::Bullet() :
	GameObject(),
	mVelocity( Vector3::Zero ),
	mMaxLinearSpeed( 50.f ),
	mMaxRotationSpeed( 5.f ),
	mWallRestitution( 0.1f ),
	mNPCRestitution( 0.1f ),
	mPlayerId( 0 ),
	mLastMoveTimestamp ( 0.0f ),
	mThrustDir( 0.f ),
	mHealth( 10 ),
	mIsShooting( false )

{
	SetCollisionRadius( 0.5f );
}
// spawns the bullet at the players position, slightly infront of the player//
void Bullet::InitFromShooter(Player* player)
{
	SetShooterID(player->GetPlayerId());

	Vector3 forward = player->GetForwardVector();
	SetVelocity(player->GetForwardVector() * mBulletSpeed);
	SetLocation(player->GetLocation() + forward * 1.5f);
	SetRotation(player->GetRotation());
}
// This Multiplies the velocity with the forward vector by the bullet speed //
void Bullet::AdjustVelocityByThrust( float inDeltaTime )
{
	//just set the velocity based on the thrust direction -- no thrust will lead to 0 velocity
	//simulating acceleration makes the client prediction a bit more complex
	Vector3 forwardVector = GetForwardVector();
	mVelocity = forwardVector * (mBulletSpeed * inDeltaTime * mMaxLinearSpeed);
}
// simulateMovement is called on the bullet server, the bullets velocity is adjusted and the new location for the bullet is set //
void Bullet::SimulateMovement( float inDeltaTime )
{
	//simulate us...
	AdjustVelocityByThrust( inDeltaTime );

	SetLocation( GetLocation() + mVelocity * inDeltaTime );

	//ProcessCollisions();
}
// will be called by the bullet server, the current delta time is taken from the time in singleton. The delta time is passed through to simulate movement//
void Bullet::Update()
{
	float deltaTime = Timing::sInstance.GetDeltaTime();
	SimulateMovement(deltaTime);
}

/*! \brief Serialises the data for example the location and velocity*/
void Bullet::Write( OutputMemoryBitStream& inOutputStream) const
{
	int shooterID = GetShooterID();
	inOutputStream.Write(shooterID);

	Vector3 velocity = mVelocity;
	inOutputStream.Write( velocity.mX );
	inOutputStream.Write( velocity.mY );

	Vector3 location = GetLocation();
	inOutputStream.Write( location.mX );
	inOutputStream.Write( location.mY );

	inOutputStream.Write( GetRotation() );
	inOutputStream.Write( mThrustDir > 0.f );
	inOutputStream.Write( GetColor() );
	inOutputStream.Write( mHealth, 4 );
}

bool Bullet::operator==(Bullet&other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if(!GameObject::operator==(other)) return false;

	if(this->ECRS_AllState != other.ECRS_AllState) return false;

	if (!Maths::Is3DVectorEqual(this->mVelocity, other.mVelocity)) return false;
	if (!Maths::FP_EQUAL(this->mMaxLinearSpeed, other.mMaxLinearSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mMaxRotationSpeed, other.mMaxRotationSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mWallRestitution, other.mWallRestitution)) return false;
	if (!Maths::FP_EQUAL(this->mNPCRestitution, other.mNPCRestitution)) return false;
	if(this->mPlayerId != other.mPlayerId) return false;

	if (!Maths::FP_EQUAL(this->mLastMoveTimestamp, other.mLastMoveTimestamp)) return false;
	if (!Maths::FP_EQUAL(this->mThrustDir, other.mThrustDir)) return false;
	if(this->mHealth != other.mHealth) return false;
	if(this->mIsShooting != other.mIsShooting) return false;

	return true;
}
// player and bullet has this, it keeps the object within the level
void Bullet::ProcessCollisionsWithScreenWalls()
{
	// some form of destroy on wall hit needed
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;

	float vx = mVelocity.mX;
	float vy = mVelocity.mY;

	float radius = GetCollisionRadius();

	//if the cat collides against a wall, the quick solution is to push it off
	if ((y + radius) >= HALF_WORLD_HEIGHT && vy > 0)

	{ 
		// sets true if object is intended to die or become damaged the next time the server checks for objects to destroy//
		SetDoesWantToDie(true);
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = HALF_WORLD_HEIGHT - radius;
		SetLocation(location);
	}
	else if (y <= (-HALF_WORLD_HEIGHT - radius) && vy < 0)
	{
		SetDoesWantToDie(true);
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = -HALF_WORLD_HEIGHT - radius;
		SetLocation(location);
	}

	if ((x + radius) >= HALF_WORLD_WIDTH && vx > 0)
	{
		SetDoesWantToDie(true);
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = HALF_WORLD_WIDTH - radius;
		SetLocation(location);
	}
	else if (x <= (-HALF_WORLD_WIDTH - radius) && vx < 0)
	{
		SetDoesWantToDie(true);
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = -HALF_WORLD_WIDTH - radius;
		SetLocation(location);
	}
}