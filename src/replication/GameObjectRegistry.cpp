#include "GameObjectRegistry.h"

#include "GameObject.h"


#include "World.h"


std::unique_ptr< GameObjectRegistry >	GameObjectRegistry::sInstance;

void GameObjectRegistry::StaticInit()
{
	sInstance.reset( new GameObjectRegistry() );
}

GameObjectRegistry::GameObjectRegistry()
{
}
// Registers a game object in the registry //
void GameObjectRegistry::RegisterCreationFunction( uint32_t inFourCCName, GameObjectCreationFunc inCreationFunction )
{
	mNameToGameObjectCreationFunctionMap[ inFourCCName ] = inCreationFunction;
}
// used by gameobjectregistry to determine if a object can be created //
bool GameObjectRegistry::CanCreate( uint32_t inFourCCName )
{
	std::unordered_map< uint32_t, GameObjectCreationFunc >::iterator it;
	it = mNameToGameObjectCreationFunctionMap.find(inFourCCName);
	//end is the element after the last
	//if find has returned this - its not there.
	return (it != mNameToGameObjectCreationFunctionMap.end());
}
// Creates the game object with the inFourCCName code passed in the function //
GameObjectPtr GameObjectRegistry::CreateGameObject( uint32_t inFourCCName )
{

	if(CanCreate(inFourCCName) == true)
	{
		GameObjectCreationFunc creationFunc = mNameToGameObjectCreationFunctionMap[ inFourCCName ];

		GameObjectPtr gameObject = creationFunc();

		//should the registry depend on the world? this might be a little weird
		//perhaps you should ask the world to spawn things? for now it will be like this
		World::sInstance->AddGameObject( gameObject );

		return gameObject;
	}
	else
	{
		throw UnknownGameObjectType();
	}
}
