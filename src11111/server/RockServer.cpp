#include "RockServer.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

RockServer::RockServer() :
	mTimeOfNextShot( 0.f ),
	mTimeBetweenShots( 0.2f )
{}

void RockServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

void RockServer::Update()
{
	Rock::Update();


	//Vector3 oldLocation = GetLocation();
	//Vector3 oldVelocity = GetVelocity();
	//float oldRotation = GetRotation();

	//ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy( GetRockId() );
	//if( client )
	//{
	//	MoveList& moveList = client->GetUnprocessedMoveList();
	//	for( const Move& unprocessedMove : moveList )
	//	{
	//		const InputState& currentState = unprocessedMove.GetInputState();
	//		float deltaTime = unprocessedMove.GetDeltaTime();
	//		ProcessInput( deltaTime, currentState );
	//		SimulateMovement( deltaTime );
	//	}

	//	moveList.Clear();
	//}

	//HandleShooting();
}

