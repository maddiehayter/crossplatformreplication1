#include "Rock.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom//
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Rock::Rock() :
	GameObject(),
	mVelocity( Vector3::Zero ),
	mMaxLinearSpeed( 50.f ),
	mMaxRotationSpeed( 5.f ),
	mWallRestitution( 0.1f ),
	mNPCRestitution( 0.1f ),
	mRockId( 0 ),
	mLastMoveTimestamp ( 0.0f ),
	mThrustDir( 0.f ),
	mHealth( 10 ),
	mIsShooting( false )

{
	SetCollisionRadius( 0.5f );
}

void Rock::ProcessCollisionsWithScreenWalls()
{
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;

	float vx = mVelocity.mX;
	float vy = mVelocity.mY;

	float radius = GetCollisionRadius();

	//if the cat collides against a wall, the quick solution is to push it off
	if( ( y + radius ) >= HALF_WORLD_HEIGHT && vy > 0 )
	{
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}
	else if( y <= ( -HALF_WORLD_HEIGHT - radius ) && vy < 0 )
	{
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = -HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}

	if( ( x + radius ) >= HALF_WORLD_WIDTH && vx > 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
	else if(  x <= ( -HALF_WORLD_WIDTH - radius ) && vx < 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = -HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
}

void Rock::Write(OutputMemoryBitStream& inOutputStream) const
{

	inOutputStream.Write(GetRockId());

	Vector3 velocity = mVelocity;
	inOutputStream.Write(velocity.mX);
	inOutputStream.Write(velocity.mY);

	Vector3 location = GetLocation();
	inOutputStream.Write(location.mX);
	inOutputStream.Write(location.mY);

	inOutputStream.Write(GetRotation());
	inOutputStream.Write(mThrustDir > 0.f);
	inOutputStream.Write(GetColor());
	inOutputStream.Write(mHealth, 4);
}


bool Rock::operator==(Rock& other)
{
	// Game Object Part.
	//Call the == of the base, ROCK reference is
	//downcast explicitly.
	if(!GameObject::operator==(other)) return false;

	if(this->ECRS_AllState != other.ECRS_AllState) return false;

	if (!Maths::Is3DVectorEqual(this->mVelocity, other.mVelocity)) return false;
	if (!Maths::FP_EQUAL(this->mMaxLinearSpeed, other.mMaxLinearSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mMaxRotationSpeed, other.mMaxRotationSpeed)) return false;
	if (!Maths::FP_EQUAL(this->mWallRestitution, other.mWallRestitution)) return false;
	if (!Maths::FP_EQUAL(this->mNPCRestitution, other.mNPCRestitution)) return false;
	if(this->mRockId != other.mRockId) return false;

	if (!Maths::FP_EQUAL(this->mLastMoveTimestamp, other.mLastMoveTimestamp)) return false;
	if (!Maths::FP_EQUAL(this->mThrustDir, other.mThrustDir)) return false;
	if(this->mHealth != other.mHealth) return false;
	if(this->mIsShooting != other.mIsShooting) return false;

	return true;
}
