#include "RockClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"

RockClient::RockClient() :
	mTimeLocationBecameOutOfSync( 0.f ),
	mTimeVelocityBecameOutOfSync( 0.f )
{
	mSpriteComponent.reset( new SpriteComponent( this ) );
	mSpriteComponent->SetTexture( TextureManager::sInstance->GetTexture( "Rock" ) );
}

void RockClient::HandleDying()
{
	Rock::HandleDying();
}


void RockClient::Update()
{
	//for now, we don't simulate any movement on the client side
	//we only move when the server tells us to move
}

void RockClient::Read( InputMemoryBitStream& inInputStream )
{
	
	uint32_t RockId;
	inInputStream.Read( RockId );
	SetRockId( RockId );
	
	float oldRotation = GetRotation();
	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();

	float replicatedRotation;
	Vector3 replicatedLocation;
	Vector3 replicatedVelocity;

	
	inInputStream.Read( replicatedVelocity.mX );
	inInputStream.Read( replicatedVelocity.mY );

	SetVelocity( replicatedVelocity );

	inInputStream.Read( replicatedLocation.mX );
	inInputStream.Read( replicatedLocation.mY );

	SetLocation( replicatedLocation );

	inInputStream.Read( replicatedRotation );
	SetRotation( replicatedRotation );

	bool thrust;

	inInputStream.Read( thrust );
	mThrustDir = thrust ? 1.f : -1.f;
	
	
	Vector3 color;
	inInputStream.Read( color );
	SetColor( color );
	
	mHealth = 0;
	inInputStream.Read( mHealth, 4 );


}
